<?php

namespace App\Services;

use App\Models\Profession;
use Illuminate\Database\Eloquent\Collection;

class ProfessionSearchService
{
    protected $params = [
        'title' => ['boost' => 2],
        'description' => ['boost' => 1],
    ];

    /**
     * @var \App\Services\SearchService
     */
    protected $searchService;

    public function __construct()
    {
        $this->searchService = new SearchService;
    }

    public function search(string $q): Collection
    {
        return $this->searchService->search($q, $this->params, Profession::class);
    }
}
