<?php

namespace App\Models;

use ElasticScoutDriverPlus\CustomSearch;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Profession extends Model
{
    use Searchable, CustomSearch;
}
