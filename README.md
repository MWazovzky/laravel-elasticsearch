# Elasticsearch introduction

## Useful links

[Elasticsearch Reference](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)
[Boolean Search](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html)
[Fuzzy Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-fuzzy-query.html)
[Laravel Scout](https://laravel.com/docs/7.x/scout)
[elastic-client](https://github.com/babenkoivan/elastic-client)
[elastic-scout-driver](https://github.com/babenkoivan/elastic-scout-driver)
[elastic-scout-driver-plus](https://github.com/babenkoivan/elastic-scout-driver-plus)
[Tutorial](https://itnext.io/the-ultimate-guide-to-elasticsearch-in-laravel-application-ee636b79419c)
