<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;

class SearchService
{
    /**
     * @var array $options, fuzzy search query default options.
     */
    protected $options = [
        'boost' => 1,
        'fuzziness' => 'AUTO',
        'max_expansions' => 50,
        'prefix_length' => 0,
        'transpositions' => true,
        'rewrite' => 'constant_score'
    ];

    /**
     * Excecute boolean search.
     */
    public function search(string $q, array $params, string $model): Collection
    {
        return $model::boolSearch()
            ->shouldRaw($this->buildQuery($q, $params))
            ->execute()
            ->models();
    }

    /**
     * Build fuzzy search query with multiple fields.
     */
    protected function buildQuery(string $q, array $params): array
    {
        $query = [];

        foreach ($params as $key => $param) {
            $item = [];
            $item['fuzzy'] = [];
            $item['fuzzy'][$key] = [];
            $item['fuzzy'][$key]['value'] = $q;

            foreach ($this->options as $option => $value) {
                $item['fuzzy'][$key][$option] = $param[$option] ?? $value;
            }

            $query[] = $item;
        }

        return $query;
    }
}
