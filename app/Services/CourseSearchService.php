<?php

namespace App\Services;

use App\Models\Course;
use Illuminate\Database\Eloquent\Collection;

class CourseSearchService
{
    protected $params = [
        'title' => ['boost' => 2],
        'description' => ['boost' => 1],
    ];

    /**
     * @var \App\Services\SearchService
     */
    protected $searchService;

    public function __construct()
    {
        $this->searchService = new SearchService;
    }

    public function search(string $q): Collection
    {
        return $this->searchService->search($q, $this->params, Course::class);
    }
}
